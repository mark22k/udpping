
class Pinger

  require 'socket'

  RECVERR_ORIGINS = {
    SO_EE_ORIGIN_NONE: 0,
    SO_EE_ORIGIN_LOCAL: 1,
    SO_EE_ORIGIN_ICMP: 2,
    SO_EE_ORIGIN_ICMP6: 3
  }

  def initialize(destination)
    possible_destinations = Addrinfo.getaddrinfo(destination, 33434, nil, Socket::SOCK_DGRAM, Socket::IPPROTO_UDP)
    possible_destinations.keep_if &:ip?
    
    raise "No address available" if possible_destinations.empty?
    
    @destination = possible_destinations.first
    
    @socket = @destination.connect
    
    if @destination.ipv6?
      @socket.setsockopt Socket::IPPROTO_IPV6, Socket::IPV6_RECVERR, 1
      @socket.setsockopt Socket::IPPROTO_IPV6, Socket::IPV6_RECVHOPLIMIT, 1
    else
      @socket.setsockopt Socket::IPPROTO_IP, Socket::IP_RECVERR, 1
      @socket.setsockopt Socket::IPPROTO_IP, Socket::IP_RECVTTL, 1
    end

    @messages = Thread::Queue.new
  end

  def send_ping(sequence)
    @messages << "ping seq=#{sequence}"
    seq_packed = [sequence].pack('N')
    # TODO: check boundaries 0-4294967295

    @socket.sendmsg seq_packed, 0

  #rescue Errno::ECONNREFUSED => e
  #  pp e
  end

  def receive_ping
    packed_seq, addrinfo, flags, *controls = @socket.recvmsg 65535, Socket::MSG_ERRQUEUE, 65535
    reply = {}

    seq = packed_seq.unpack('N')
    if seq.empty?
      reply[:seq] = '?'
    else
      reply[:seq] = seq[0]
    end

    controls.each do |control|
      if control.cmsg_is?(Socket::IPPROTO_IP, Socket::IP_TTL) || control.cmsg_is?(Socket::IPPROTO_IPV6, Socket::IPV6_HOPLIMIT)
        ttl = control.data.unpack('L')
        if ttl.empty?
          reply[:ttl] = '?'
        else
          reply[:ttl] = ttl[0]
        end
      elsif control.cmsg_is?(Socket::IPPROTO_IP, Socket::IP_RECVERR) || control.cmsg_is?(Socket::IPPROTO_IPV6, Socket::IPV6_RECVERR)
        errno, origin, type, code, pad, info, data = control.data.unpack('LCCCCLL')
        if origin == RECVERR_ORIGINS[:SO_EE_ORIGIN_NONE]
          reply[:origin] = "none"
        elsif origin == RECVERR_ORIGINS[:SO_EE_ORIGIN_LOCAL]
          reply[:origin] = "local"
        else
          reply[:origin] = "icmp"
          if errno == Errno::EHOSTUNREACH::Errno
            address = Addrinfo.new(control.data[16..])
            reply[:icmp] = 'HOSTUNREACH'
            reply[:address] = address.ip_address
          elsif errno == Errno::ECONNREFUSED::Errno
            address = Addrinfo.new(control.data[16..])
            reply[:icmp] = 'CONNREFUSED'
            reply[:address] = address.ip_address
          elsif errno == Errno::EMSGSIZE::Errno
              address = Addrinfo.new(control.data[16..])
              reply[:mtu] = info
              reply[:icmp] = 'MSGSIZE'
              reply[:address] = address.ip_address
          else
            # If an unknown error message is returned, this still needs to be implemented.
            warn "Unknown errno: #{HopTracker::errno_name errno} / #{control.inspect}"
          end
        end
      end
    end

    message = "reply"
    reply.each do |name, value|
      message += " #{name}=#{value}"
    end
    @messages << message

  end

  def print_ping
    return @messages.pop
  end

end

pinger = Pinger.new('172.20.129.3')

send_thead = Thread.new(pinger) do |tpinger|
  counter = 0
  loop do
    tpinger.send_ping counter
    counter += 1
    counter = 0 if counter >= 4294967295
    sleep 1
  end
end

receive_thread = Thread.new(pinger) do |tpinger|
  loop do
    tpinger.receive_ping
  end
end

print_thread = Thread.new(pinger) do |tpinger|
  loop do
    puts tpinger.print_ping
  end
end


[send_thead, receive_thread, print_thread].each &:join
